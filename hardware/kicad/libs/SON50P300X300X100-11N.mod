PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
SON50P300X300X100-11N
$EndINDEX
$MODULE SON50P300X300X100-11N
Po 0 0 0 15 00000000 00000000 ~~
Li SON50P300X300X100-11N
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 0.44071 -2.53913 1.00163 1.00163 0 0.05 N V 21 "SON50P300X300X100-11N"
T1 1.07504 2.46507 1.00003 1.00003 0 0.05 N V 21 "VAL**"
DS -1.55 1.55 1.55 1.55 0.127 27
DS 1.55 1.55 1.55 -1.55 0.127 27
DS 1.55 -1.55 -1.55 -1.55 0.127 27
DS -1.55 -1.55 -1.55 1.55 0.127 27
DP 0 0 0 0 4 0 19
Dl -0.500962 -0.8
Dl 0.5 -0.8
Dl 0.5 0.80154
Dl -0.500962 0.80154
DS 1.55 -1.55 -1.55 -1.55 0.127 21
DS -1.55 1.55 1.55 1.55 0.127 21
DS -1.55 -1.55 -1.55 -1.345 0.127 21
DS 1.55 -1.55 1.55 -1.345 0.127 21
DS 1.55 1.55 1.55 1.345 0.127 21
DS -1.55 1.55 -1.55 1.345 0.127 21
DC -2.35 -1.2 -2.25 -1.2 0.2 21
DC -2.35 -1.2 -2.25 -1.2 0.2 27
DS -2.12 -1.8 2.12 -1.8 0.05 26
DS 2.12 -1.8 2.12 1.8 0.05 26
DS 2.12 1.8 -2.12 1.8 0.05 26
DS -2.12 1.8 -2.12 -1.8 0.05 26
$PAD
Sh "1" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.435 -1
$EndPAD
$PAD
Sh "2" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.435 -0.5
$EndPAD
$PAD
Sh "3" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.435 0
$EndPAD
$PAD
Sh "4" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.435 0.5
$EndPAD
$PAD
Sh "5" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.435 1
$EndPAD
$PAD
Sh "6" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.435 1
$EndPAD
$PAD
Sh "7" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.435 0.5
$EndPAD
$PAD
Sh "8" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.435 0
$EndPAD
$PAD
Sh "9" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.435 -0.5
$EndPAD
$PAD
Sh "10" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.435 -1
$EndPAD
$PAD
Sh "11" R 1.575 2.45 0 0 0
At SMD N 00888000
.SolderMask 0
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE SON50P300X300X100-11N
