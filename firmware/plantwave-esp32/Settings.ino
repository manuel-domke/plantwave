
void loadFactorySettings() {
#ifdef REV8
  preferences.putInt("hwrev", 8);
#else
  preferences.putInt("hwrev", 7);
#endif
  preferences.putInt("channel", 1);
  preferences.putInt("root", 0);
  preferences.putInt("controlNumber", 80);
  preferences.putBool("ble-state", true);
  preferences.putBool("wifi-state", true);
  preferences.putInt("usb-mode", 0);
  preferences.putInt("led-mode", 1);
  preferences.putInt("led-bright", 255);
  preferences.putInt("sensitivity", 80);
  preferences.remove("ssid");
  preferences.remove("psk");
  preferences.putBool("plantwave-init", true);
  preferences.putString("name", "PlantWave");
  preferences.putInt("scale", 0);
  preferences.putInt("poly", 5);
  clearpairing();
  blinkStatusLED(5, 500);
  delay(5000);
}

void readSettings() {
  preferences.begin("dev", false);

#ifdef REV8
  preferences.putInt("hwrev", 8);
#endif

  // check if esp32 is naked or not
  if (!preferences.getBool("plantwave-init")) {
    loadFactorySettings();
  }

  // check if button is hold down for > 10s (to reset EEPROM settings)
  delay(100);
  if (digitalRead(BUTTON_PIN) == LOW) {
    delay(1000);
    if (digitalRead(BUTTON_PIN) == LOW) {
      delay(9000);
      if (digitalRead(BUTTON_PIN) == LOW) {
        loadFactorySettings();
      }
    }
  }

  // load settings
  channel = preferences.getInt("channel", 1);
  ble_enabled = preferences.getBool("ble-state", true);
  wifi_enabled = preferences.getBool("wifi-state", true);
  controlNumber = preferences.getInt("controlNumber", 80);
  currScale = preferences.getInt("scale", 1);
  setThreshold(preferences.getInt("sensitivity", 80));
  midiMode = preferences.getInt("usb-mode", 0);
  noteLEDs = preferences.getInt("led-mode", 1);
  maxBrightness = preferences.getInt("led-bright", 255);
  root = preferences.getInt("root", 0);
  polyphony =  preferences.getInt("poly", 5);
  preferences.getString("name", "PlantWave").toCharArray(device_name, 24);
#ifdef REV8
  hwrev = preferences.getInt("hwrev", 8);
#else
  hwrev = preferences.getInt("hwrev", 7);
#endif
}
