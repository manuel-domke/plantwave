/*------------
  MIDI_PsychoGalvanometer v2.0
  Accepts pulse inputs from a Galvanic Conductance sensor
  consisting of a 555 timer set as an astablemultivibrator and two electrodes.
  Through sampling pulse widths and identifying fluctuations, MIDI note and control messages
  are generated.  Features include Threshold, Scaling, Control Number, and Control Voltage
  using PWM through an RC Low Pass filte

  r.
  MIDIsprout.com

  modified by 13-37.org:
  - added EEPROM support and menu functionality from the Arduino shield version https://github.com/electricityforprogress/BiodataSonificationBreadboardKit
  -------------*/
#include <Arduino.h>
#include <string>
#include <Preferences.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <WiFi.h>
#include <ESPmDNS.h>
#include <AppleMidi.h>
#include <LEDFader_esp32.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <BLE2902.h>
#include "esp_bt.h"
#include "esp_wifi.h"
#include <WebServer.h>
#include "version.h"

#define LED_NUM 5
LEDFader leds[LED_NUM] = { // 6 LEDs (perhaps 2 RGB LEDs)
  LEDFader(5), // R
  LEDFader(4), // Y
  LEDFader(3), // G
  LEDFader(2), // B
  LEDFader(1) // W
};


#define SO_REUSEADDR

#define INTERRUPT_PIN 13 //galvanometer input
#define LATCH_PIN 5
#define BUTTON_PIN 18
#define USB_MODE_PIN 16
#define STATUS_LED 23

#ifdef REV3
#define LED_R_PIN 27
#define LED_G_PIN 26
#define LED_B_PIN 25
#define LED_Y_PIN 33
#define LED_W_PIN 32
#else
//REV4+
#define LED_R_PIN 12
#define LED_G_PIN 27
#define LED_B_PIN 33
#define LED_Y_PIN 14
#define LED_W_PIN 32
#endif



//#define SAMPLEDEBUG

unsigned long batteryCheck = 10000; //battery check delay timer
unsigned long bleStatusLineInterval = 10000; //send device status over BLE UART every 10s

#define WIFI_CONNECT_TIMEOUT    15
#define SMART_CONFIG_TIMEOUT    3

char device_name[24] = "PlantWave";

#define BUTTON_DEBOUNCE 100
#define SECURE_UPDATE

#define OTA_UPDATE_BASE_URL "fwupdate.datagardencdn.com/plantwave/"

#define PLANTWAVE_SERVICE_UUID           "9f8e74cc-4d4f-4796-8669-33513370dd7a"
#define PLANTWAVE_ID_CHARACTERISTIC_UUID "1aa5bc83-d31c-4dc3-8315-d513379cc40c"

#define BLE_UART_SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"
#define BLE_UART_CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define BLE_UART_CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"

#define BLE_MIDI_SERVICE_UUID        "03b80e5a-ede8-4b33-a751-6ce34ec4c700"
#define BLE_MIDI_CHARACTERISTIC_UUID "7772e5db-3868-4112-a1a9-f2669d106bf3"

#define STATUS_LED_OFF()  digitalWrite(STATUS_LED, LOW)
#define STATUS_LED_ON()   digitalWrite(STATUS_LED, HIGH)

//AppleMIDI
APPLEMIDI_CREATE_INSTANCE(WiFiUDP, AppleMIDI); // see definition in AppleMidi_Defs.h
bool          appleMidiConnected = false;

// IPMIDI
WiFiUDP                 ipMIDI;
IPAddress               ipMIDImulticast(225, 0, 0, 37);
unsigned int            ipMIDIdestPort = 21928;
bool ipMIDIEnabled = false;

// BLE
bool bleDeviceConnected = false;
BLECharacteristic *midiCharacteristic;
BLECharacteristic *uartTXCharacteristic;

// HTTP-Server
WebServer server(80);

// Settings
Preferences preferences;
//******************************
//set scaled values, sorted array, first element scale length
const int scaleCount = 6;
const int scaleLen = 13; //maximum scale length plus 1 for 'used length'

uint16_t currScale = 0; //current scale, default Chrom // SETTING
uint16_t maxBrightness = 190; // SETTING
uint8_t channel = 1;  //setting channel to 11 or 12 often helps simply computer midi routing setups // SETTING
uint8_t sensitivity = 80;

#define MIDI_MODE_DISABLED 0
#define MIDI_MODE_ENABLED 1
#define MIDI_MODE_USB 2

int midiMode = MIDI_MODE_DISABLED;

bool wifi_enabled = true;
bool ble_enabled = true;

float threshold = 3;   //2.3;  //change threshold multiplier // SETTING
float threshMin = 1.61; //scaling threshold min
float threshMax = 3.91; //scaling threshold max

int scale[scaleCount][scaleLen] = {
  {12, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, // Chromatic
  {7, 0, 2, 4, 5, 7, 9, 11}, //Major
  {7, 0, 1, 1, 4, 5, 8, 10}, //Indian
  {7, 0, 2, 3, 5, 7, 8, 10}, //Minor
  {5, 0, 2, 4, 7, 9}, // Pentatonic Major
  {5, 0, 3, 5, 7, 10} // Pentatonic Minor
};

// 1 midi cc
// scales
// scale root
int root = 0; //initialize for root, pitch shifting
int pulseRate = 350; //base pulse rate

const byte samplesize = 10; //set sample array size
const byte analysize = samplesize - 1;  //trim for analysis array

uint8_t polyphony = 5; //above 8 notes may run out of ram
#define MAX_POLYPHONY 8

int noteMin = 36; //C2  - keyboard note minimum
int noteMax = 96; //C7  - keyboard note maximum
byte QY8 = 0; //sends each note out chan 1-4, for use with General MIDI like Yamaha QY8 sequencer
int controlNumber = 80; //set to mappable control, low values may interfere with other soft synth controls!!
//byte controlVoltage = 1; //output PWM CV on controlLED, pin 17, PB3, digital 11 *lowpass filter
float batteryLimit = 3.2; //voltage check minimum, power off to prevent battery deep discharge

int batterySamples[3];
uint8_t bat_index = 0;
float batVoltage = 0;

int value = 0;
int prevValue = 0;

volatile unsigned long microseconds; //sampling timer
volatile byte index1 = 0;
volatile unsigned long samples[samplesize];
unsigned long currentMillis = 1;
unsigned long previousBlinkMillis = 0;
unsigned long previousButtonMillis = 0;
bool pairingMode = false;


byte controlLED = 5; //array index1 of control LED (CV out)
byte noteLEDs = 1;  //performs lightshow set at noteOn event

typedef struct _MIDImessage { //build structure for Note and Control MIDImessages
  unsigned int type;
  int value;
  int velocity;
  long duration;
  long period;
  int channel;
}

MIDImessage;
MIDImessage noteArray[MAX_POLYPHONY]; //manage MIDImessage data as an array with size polyphony
int noteindex1 = 0;
MIDImessage controlMessage; //manage MIDImessage data for Control Message (CV out)

int hwrev = 0;
//provide float map function
float mapfloat(float x, float in_min, float in_max, float out_min, float out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
void setThreshold(int percentage) {
  threshold = mapfloat(percentage, 0, 100, threshMax, threshMin);
  if (midiMode == MIDI_MODE_USB) {
    //Serial.print("new threshold: ");
    //Serial.println(threshold);
  }
  sensitivity = percentage;
}

//interrupt timing sample array
void IRAM_ATTR isr() {
  if (index1 < samplesize) {
    samples[index1] = micros() - microseconds;
    microseconds = samples[index1] + microseconds; //rebuild micros() value w/o recalling
    index1 += 1;
  }
}

void setup()
{
  pinMode(STATUS_LED, OUTPUT);
  digitalWrite(STATUS_LED, HIGH);

#ifdef REV8
  pinMode(BUTTON_PIN, INPUT_PULLUP);
#else
  pinMode(BUTTON_PIN, INPUT);
  pinMode(LATCH_PIN, OUTPUT);
  digitalWrite(LATCH_PIN, HIGH);
#endif

  initBatteryCheck();
  if (batVoltage < batteryLimit) {
    blinkLED(2, 1);
  }

  readSettings();

  //if usbmode is True, USB Mode = MIDI, otherwise Serial
  pinMode(USB_MODE_PIN, OUTPUT);

  if (midiMode == MIDI_MODE_ENABLED) {
    Serial.begin(31250); // MIDI
    digitalWrite(USB_MODE_PIN, 1);
  } else if (midiMode == MIDI_MODE_USB) {
    Serial.begin(500000); // Serial
    Serial.println("PlantWave started in USB Serial/Debug mode");
    digitalWrite(USB_MODE_PIN, 0);
  } else {
    digitalWrite(USB_MODE_PIN, 0);
    // no serial
  }

  attachInterrupt(INTERRUPT_PIN, isr, RISING);  //begin sampling from interrupt

  int freq = 500;
  int resolution = 8;
  ledcSetup(1, freq, resolution);
  ledcSetup(2, freq, resolution);
  ledcSetup(3, freq, resolution);
  ledcSetup(4, freq, resolution);
  ledcSetup(5, freq, resolution);

  ledcAttachPin(LED_R_PIN, 5);
  ledcAttachPin(LED_Y_PIN, 4);
  ledcAttachPin(LED_G_PIN, 3);
  ledcAttachPin(LED_B_PIN, 2);
  ledcAttachPin(LED_W_PIN, 1);

  if (wifi_enabled) {
    ////DPRINTLN("Connecting to WiFI");
    WiFi.persistent(false);
    WiFi.onEvent(WiFiEvent);
    wifi_connect();
    checkForUpdateFlag();
    setupHTTPServer();
  }

  if (ble_enabled) {
    startBLE();
  }

  bootLightshow(); //a light show to display on system boot
  controlMessage.value = 0;  //begin CV at 0
}

bool isValidDeviceName(String instr) {
  const char * msg = instr.c_str();
  if (strlen(msg) > 24) {
    return false;
  }
  for (int i = 0; i < strlen(msg); i++) {
    if (isalpha(msg[i])) {
      continue;
    }
    if (isdigit(msg[i])) {
      continue;
    }
    if (msg[i] == '.') {
      continue;
    }
    if (msg[i] == '_') {
      continue;
    }
    if (msg[i] == '-') {
      continue;
    }
    return false;
  }
  return true;
}


void fadeOn(int ledNum) {
  LEDFader *led = &leds[ledNum];
  led->fade(255, 1000);
  while (led->is_fading()) {
    led->update();
  }
}

void blinkLED(int ledNum, int count) {
  LEDFader *led = &leds[ledNum];
  for (int i = 0; i < count; i++) {
    led->fade(255, 500);
    while (led->is_fading()) {
      led->update();
    }
    led->fade(0, 500);
    while (led->is_fading()) {
      led->update();
    }
  }
}

void toggleWifi() {
  if (wifi_enabled) {
    disableWifi();
  } else {
    enableWifi();
  }
}

void loop() {
  //timerWrite(timer, 0); //reset timer (feed watchdog)
  server.handleClient();
  AppleMIDI.run();
  currentMillis = millis();   //manage time
  checkBattery(); //on low power, shutoff lightShow, continue MIDI operation
  checkButton();
  if (index1 >= samplesize)  {
    analyzeSample();  //if samples array full, also checked in analyzeSample(), call sample analysis
  }

  checkNote();  //turn off expired notes
  checkControl();  //update control value
  checkLED();  //LED management without delay()
  controlStatusLED();
  sendBLEStatusInfo();
  //delayMicroseconds(500);
}



void blinkStatusLED(int count, int dur) {
  for (int i = 0 ; i < count; i++) {
    digitalWrite(STATUS_LED, LOW);
    delay(dur);
    digitalWrite(STATUS_LED, HIGH);
    delay(dur);
  }
}


void setNote(int value, int velocity, long duration, int notechannel) {
  //find available note in array (velocity = 0);
  for (int i = 0; i < polyphony; i++) {
    if (!noteArray[i].velocity) {
      //if velocity is 0, replace note in array
      noteArray[i].type = 0;
      noteArray[i].value = value;
      noteArray[i].velocity = velocity;
      noteArray[i].duration = currentMillis + duration;
      noteArray[i].channel = notechannel;

      midiSerial(144, channel, value, velocity);

      if (noteLEDs == 1) { //normal mode
        for (byte j = 0; j < (LED_NUM - 1); j++) { //find available LED and set
          if (!leds[j].is_fading()) {
            rampUp(i, maxBrightness, duration);
            break;
          }
        }
      } else if (noteLEDs == 2) { //threshold special display mode
        for (byte j = 1; j < (LED_NUM - 1); j++) { //find available LED above first and set
          if (!leds[j].is_fading()) {
            rampUp(i, maxBrightness, duration);
            break;
          }
        }
      }
      break;
    }
  }
}

void setControl(int type, int value, int velocity, long duration)
{
  controlMessage.type = type;
  controlMessage.value = value;
  controlMessage.velocity = velocity;
  controlMessage.period = duration;
  controlMessage.duration = currentMillis + duration; //schedule for update cycle
}


void checkControl()
{
  //need to make this a smooth slide transition, using high precision
  //distance is current minus goal
  signed int distance =  controlMessage.velocity - controlMessage.value;
  //if still sliding
  if (distance != 0) {
    //check timing
    if (currentMillis > controlMessage.duration) { //and duration expired
      controlMessage.duration = currentMillis + controlMessage.period; //extend duration
      //update value
      if (distance > 0) {
        controlMessage.value += 1;
      } else {
        controlMessage.value -= 1;
      }

      //send MIDI control message after ramp duration expires, on each increment
      midiSerial(176, channel, controlMessage.type, controlMessage.value);

      //send out control voltage message on pin 17, PB3, digital 11
      /*if (controlVoltage) {
        if (distance > 0) {
          rampUp(controlLED, map(controlMessage.value, 0, 127, 0 , 255), 5);
        }
        else {
          rampDown(controlLED, map(controlMessage.value, 0, 127, 0 , 255), 5);
        }
        }*/
    }
  }
}

void checkNote()
{
  for (uint8_t i = 0; i < polyphony; i++) {
    if (noteArray[i].velocity) {
      if (noteArray[i].duration <= currentMillis) {
        //send noteOff for all notes with expired duration
        if (QY8) {
          midiSerial(144, noteArray[i].channel, noteArray[i].value, 0);
        }
        else {
          midiSerial(144, channel, noteArray[i].value, 0);
        }
        noteArray[i].velocity = 0;
        if (noteLEDs == 1) rampDown(i, 0, 225);
        if (noteLEDs == 2) rampDown(i + 1, 0, 225); //special threshold display mode
      }
    }
  }
}

void MIDIpanic()
{
  //120 - all sound off
  //123 - All Notes off
  // midiSerial(21, panicChannel, 123, 0); //123 kill all notes

  //brute force all notes Off
  for (byte i = 1; i < 128; i++) {
    delay(1); //don't choke on note offs!
    midiSerial(144, channel, i, 0); //clear notes on main channel
  }
}

uint8_t midiPacket[] = {
  0x80,  // header
  0x80,  // timestamp, not implemented
  0x00,  // status
  0x3c,  // 0x3c == 60 == middle c
  0x00   // velocity
};

byte ipmidiPacket[3];

void midiSerial(int type, int channel, uint8_t data1, int data2) {
  //  Note type = 144
  //  Control type = 176
  // remove MSBs on data
  data1 &= 0x7F;  //number
  data2 &= 0x7F;  //velocity
  byte statusbyte = (type | ((channel - 1) & 0x0F));

  if (midiMode == MIDI_MODE_ENABLED) { // MIDI-Mode
    Serial.write(statusbyte);
    Serial.write(data1);
    Serial.write(data2);
  } else if (midiMode == MIDI_MODE_USB) {  // Serial/Debug Mode
    Serial.println(statusbyte);
    Serial.println(data1);
    Serial.println(data2);
  }


  if (ble_enabled && bleDeviceConnected) {
    ////DPRINTLN("Sending BLE packet");
    midiPacket[2] = statusbyte;
    midiPacket[3] = data1; // note down, channel 0
    midiPacket[4] = data2;  // velocity
    midiCharacteristic->setValue(midiPacket, 5); // packet, length in bytes
    midiCharacteristic->notify();

    //uartTXCharacteristic->setValue(&data1, 1);
    //uartTXCharacteristic->notify();

    delay(10);
  }

  if (wifi_enabled && appleMidiConnected) {
    ////DPRINTLN("Sending AppleMIDI packet");
    if (type == 144) {
      AppleMIDI.noteOn(data1, data2, channel);
    }
    if (type == 176) {
      AppleMIDI.controlChange(data1, data2, channel);
    }
  }

  if (wifi_enabled && ipMIDIEnabled) {
    ////DPRINTLN("Sending MIDI packet (ipMIDI)");
    ipmidiPacket[0] = (type & 0xf0) | ((channel - 1) & 0x0f);
    ipmidiPacket[1] = data1;
    ipmidiPacket[2] = data2;
    ipMIDI.beginMulticastPacket();
    ipMIDI.write(ipmidiPacket, 3);
    ipMIDI.endPacket();
  }
}

int ledBrightness[] = {64,255,255,64, 127}; // RYGBW

void rampUp(int ledPin, int value, int time) {
  LEDFader *led = &leds[ledPin];
  //scale the value parameter against a new maxBrightness global variable
  led->fade(value, time);
  led->fade(ledBrightness[ledPin], time);
}

void rampDown(int ledPin, int value, int time) {
  LEDFader *led = &leds[ledPin];
  led->fade(value, time); //fade out
}

void checkLED() {
  //iterate through LED array and call update
  for (byte i = 0; i < LED_NUM; i++) {
    LEDFader *led = &leds[i];
    led->update();
  }
}


void controlStatusLED() {
  if (pairingMode) {
    if ((currentMillis - previousButtonMillis) < 30000) {
      // all good, pairing mode active, blink!
      if ((currentMillis - previousBlinkMillis) > 1000) {
        digitalWrite(STATUS_LED, !digitalRead(STATUS_LED));
        previousBlinkMillis = currentMillis;
      }
    } else {
      leavePairingMode();
    }
    return;
  }

  if (wifi_enabled) {
    digitalWrite(STATUS_LED, HIGH);
  } else {
    if (digitalRead(STATUS_LED) == HIGH) {
      if ((currentMillis - previousBlinkMillis) > 100) {
        digitalWrite(STATUS_LED, LOW);
        previousBlinkMillis = currentMillis;
      }
    } else {
      if ((currentMillis - previousBlinkMillis) > 3000) {
        digitalWrite(STATUS_LED, HIGH);
        previousBlinkMillis = currentMillis;
      }
    }
  }
}

void checkButton() {
  if (digitalRead(BUTTON_PIN) == LOW) {
    delay(250);
    if (digitalRead(BUTTON_PIN) == HIGH) {
      delay(100);
      if (digitalRead(BUTTON_PIN) == HIGH) { // button released after ~150ms
        blinkStatusLED(3, 50);
        enterPairingMode();
        previousButtonMillis = currentMillis;
        previousBlinkMillis = currentMillis;
        return;
      }
    }

    if (digitalRead(BUTTON_PIN) == LOW) {
      delay(3000);
      if (digitalRead(BUTTON_PIN) == LOW) {
        blinkStatusLED(3, 50);
        toggleWifi();
        previousButtonMillis = currentMillis;
        previousBlinkMillis = currentMillis;
      }
    }
  }
}



void initBatteryCheck() {
  batterySamples[0] = analogRead(36);
  delay(10);
  batterySamples[1] = analogRead(36);
  delay(10);
  batterySamples[2] = analogRead(36);
  calculateBatVoltage();
}

void calculateBatVoltage() {
  int avg = (batterySamples[0] + batterySamples[1] + batterySamples[2]) / 3;
  batVoltage = mapfloat(avg, 0, 4095.0f, 0, 7.3f); // 100K resistor divider, VCC=3,29V
}

float calculateBatPercentage() {
  return mapfloat(float(batVoltage), 3.15, 4.1, 0, 100);
}

void checkBattery() {
  if (batteryCheck < currentMillis) {
    batteryCheck = currentMillis + 3000; //reset timer for next battery check
    if (bat_index == 2) {
      calculateBatVoltage();
      if (batVoltage < batteryLimit) { // battery empty, shutdown
        //powerDown();
        blinkLED(0, 5); // fade red LED ON
      }
      // reset index and sample
      bat_index = 0;
      batterySamples[bat_index] = analogRead(36);
    } else { // collect more samples
      bat_index++;
      batterySamples[bat_index] = analogRead(36);
    }
  }
}

void pulse(int ledPin, int maxValue, int time) {
  LEDFader *led = &leds[ledPin];
  //check on the state of the LED and force it to pulse
  if (led->is_fading() == false) { //if not fading
    if (led->get_value() > 0) { //and is illuminated
      led->fade(0, time); //fade down
    } else {
      led->fade(maxValue, time); //fade up
    }
  }
}

void bootLightshow() {
  //light show to be displayed on boot
  for (byte i = 0; i <= 5; i++) {
    LEDFader *led = &leds[i - 1];
    //led->set_value(200); //set to max

    led->fade(200, 150); //fade up
    while (led->is_fading()) checkLED();

    led->fade(0, 150 + i * 17); //fade down
    while (led->is_fading()) checkLED();
    //move to next LED
  }
}

void analyzeSample()
{
  //eating up memory, one long at a time!
  unsigned long averg = 0;
  unsigned long maxim = 0;
  unsigned long minim = 100000;
  float stdevi = 0;
  unsigned long delta = 0;
  byte change = 0;
  bool idle = true;

  if (index1 == samplesize) { //array is full
    unsigned long sampanalysis[analysize];
    for (byte i = 0; i < analysize; i++) {
      //skip first element in the array
      sampanalysis[i] = samples[i + 1]; //load analysis table (due to volitle)

#ifdef SAMPLEDEBUG
      Serial.printf("sample %i: %u\n", i, sampanalysis[i]);
      Serial.flush();
#endif

      if (sampanalysis[i] < 100000) {
        idle = false;
      }

      //manual calculation
      if (sampanalysis[i] > maxim) {
        maxim = sampanalysis[i];
      }
      if (sampanalysis[i] < minim) {
        minim = sampanalysis[i];
      }
      averg += sampanalysis[i];
      stdevi += sampanalysis[i] * sampanalysis[i];  //prep stdevi
    }

    if (idle) {
      index1 = 0;
      return;
    }

    //manual calculation
    averg = averg / analysize;
    stdevi = pow(stdevi / analysize - averg * averg, 0.5); //calculate stdevi
    if (stdevi < 1) {
      stdevi = 1.0;  //min stdevi of 1
    }

    delta = maxim - minim;

    //**********perform change detection
    if (delta > (stdevi * threshold)) {
      change = 1;
    }
    //*********

    if (change) { // set note and control vector
      int dur = 150 + (map(delta % 127, 1, 127, 100, 2500)); //length of note
      int ramp = 3 + (dur % 100); //control slide rate, min 25 (or 3 ;)
      int notechannel = random(1, 5); //gather a random channel for QY8 mode

      //set scaling, root key, note
      int setnote = map(averg % 127, 1, 127, noteMin, noteMax); //derive note, min and max note
      setnote = scaleNote_fast(setnote, root);  //scale the note
      //setnote = scaleNote_slow(setnote, scale[currScale], root);  //scale the note
      // setnote = setnote + root; // (apply root?)

      if (QY8) {
        setNote(setnote, 100, dur, notechannel);  //set for QY8 mode
      } else {
        setNote(setnote, 100, dur, channel);
      }

      //derive control parameters and set
      setControl(controlNumber, controlMessage.value, delta % 127, ramp); //set the ramp rate for the control
    }

    //reset array for next sample
    index1 = 0;
  }

#ifdef SAMPLEDEBUG
  Serial.printf("delta: %u\n", delta);
  Serial.printf("averg: %u\n", averg);
  Serial.printf("stdevi: %f\n", stdevi);
  Serial.printf("minim: %u\n", minim);
  Serial.printf("maxim: %u\n", maxim);
  Serial.printf("change: %u\n", change);
#endif
}

int scaleSearch(int note, int scale[], int scalesize) {
  for (byte i = 1; i <= scalesize; i++) {
    if (note == scale[i]) {
      return note;
    } else {
      if (note < scale[i]) {
        return scale[i];  //highest scale value less than or equal to note
      }
    }
    //otherwise continue search
  }
  //didn't find note and didn't pass note value, uh oh!
  return 6; //give arbitrary value rather than fail
}

int scaleNote_fast(int note, int root) {
  //input note mod 12 for scaling, note/12 octave
  //search array for nearest note, return scaled*octave
  int scaled = note % 12;
  int octave = note / 12;
  int scalesize = (scale[currScale][0]);
  //search entire array and return closest scaled note
  scaled = scaleSearch(scaled, scale[currScale], scalesize);
  scaled = (scaled + (12 * octave)) + root; //apply octave and root
  return scaled;
}

void save_wifi_credentials(String ssid, String password)
{
  preferences.putString("ssid", ssid);
  preferences.putString("psk", password);
  ////DPRINTLN("WiFi credentials saved into EEPROM");
}

void status_blink()
{
  STATUS_LED_OFF();
  delay(50);
  STATUS_LED_ON();
}
