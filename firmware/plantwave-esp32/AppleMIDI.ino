

void OnAppleMidiConnected(uint32_t ssrc, char* name)
{
  appleMidiConnected  = true;
  ////DPRINTLN("AppleMIDI Connected Session ID: %u Name: %s", ssrc, name);
}
void OnAppleMidiDisconnected(uint32_t ssrc)
{
  appleMidiConnected  = true;
  ////DPRINTLN("AppleMIDI disconnected Session ID: %u", ssrc);
}

void apple_midi_start()
{
  // Create a session and wait for a remote host to connect to us
  AppleMIDI.begin(device_name);

  AppleMIDI.OnConnected(OnAppleMidiConnected);
  AppleMIDI.OnDisconnected(OnAppleMidiDisconnected);
}

void WiFiEvent(WiFiEvent_t event) {
  IPAddress localMask;
  /*
      ESP32 events
    SYSTEM_EVENT_WIFI_READY               < ESP32 WiFi ready
    SYSTEM_EVENT_SCAN_DONE                < ESP32 finish scanning AP
    SYSTEM_EVENT_STA_START                < ESP32 station start
    SYSTEM_EVENT_STA_STOP                 < ESP32 station stop
    SYSTEM_EVENT_STA_CONNECTED            < ESP32 station connected to AP
    SYSTEM_EVENT_STA_DISCONNECTED         < ESP32 station disconnected from AP
    SYSTEM_EVENT_STA_AUTHMODE_CHANGE      < the auth mode of AP connected by ESP32 station changed
    SYSTEM_EVENT_STA_GOT_IP               < ESP32 station got IP from connected AP
    SYSTEM_EVENT_STA_LOST_IP              < ESP32 station lost IP and the IP is reset to 0
    SYSTEM_EVENT_STA_WPS_ER_SUCCESS       < ESP32 station wps succeeds in enrollee mode
    SYSTEM_EVENT_STA_WPS_ER_FAILED        < ESP32 station wps fails in enrollee mode
    SYSTEM_EVENT_STA_WPS_ER_TIMEOUT       < ESP32 station wps timeout in enrollee mode
    SYSTEM_EVENT_STA_WPS_ER_PIN           < ESP32 station wps pin code in enrollee mode
    SYSTEM_EVENT_AP_START                 < ESP32 soft-AP start
    SYSTEM_EVENT_AP_STOP                  < ESP32 soft-AP stop
    SYSTEM_EVENT_AP_STACONNECTED          < a station connected to ESP32 soft-AP
    SYSTEM_EVENT_AP_STADISCONNECTED       < a station disconnected from ESP32 soft-AP
    SYSTEM_EVENT_AP_PROBEREQRECVED        < Receive probe request packet in soft-AP interface
    SYSTEM_EVENT_GOT_IP6                  < ESP32 station or ap or ethernet interface v6IP addr is preferred
    SYSTEM_EVENT_ETH_START                < ESP32 ethernet start
    SYSTEM_EVENT_ETH_STOP                 < ESP32 ethernet stop
    SYSTEM_EVENT_ETH_CONNECTED            < ESP32 ethernet phy link up
    SYSTEM_EVENT_ETH_DISCONNECTED         < ESP32 ethernet phy link down
    SYSTEM_EVENT_ETH_GOT_IP               < ESP32 ethernet got IP from connected AP
    SYSTEM_EVENT_MAX
  */
  switch (event) {
    case SYSTEM_EVENT_STA_START:
      ////DPRINTLN("SYSTEM_EVENT_STA_START");
      break;

    case SYSTEM_EVENT_STA_STOP:
      ////DPRINTLN("SYSTEM_EVENT_STA_STOP");
      break;

    case SYSTEM_EVENT_WIFI_READY:
      ////DPRINTLN("SYSTEM_EVENT_WIFI_READY");
      break;

    case SYSTEM_EVENT_STA_CONNECTED:
      ////DPRINTLN("SYSTEM_EVENT_STA_CONNECTED");
      uint8_t macAddr[6];
      WiFi.macAddress(macAddr);
      ////DPRINTLN("BSSID       : %s", WiFi.BSSIDstr().c_str());
      ////DPRINTLN("RSSI        : %d dBm", WiFi.RSSI());
      ////DPRINTLN("Channel     : %d", WiFi.channel());
      ////DPRINTLN("STA         : %02X:%02X:%02X:%02X:%02X:%02X", macAddr[0], macAddr[1], macAddr[2], macAddr[3], macAddr[4], macAddr[5]);
      WiFi.setHostname(device_name);
      break;

    case SYSTEM_EVENT_STA_GOT_IP:
      ////DPRINTLN("SYSTEM_EVENT_STA_GOT_IP");
      ////DPRINTLN("Hostname    : %s", WiFi.getHostname());
      ////DPRINTLN("IP address  : %s", WiFi.localIP().toString().c_str());
      ////DPRINTLN("Subnet mask : %s", WiFi.subnetMask().toString().c_str());
      ////DPRINTLN("Gataway IP  : %s", WiFi.gatewayIP().toString().c_str());
      ////DPRINTLN("DNS 1       : %s", WiFi.dnsIP(0).toString().c_str());
      ////DPRINTLN("DNS 2       : %s", WiFi.dnsIP(1).toString().c_str());

      // Start mDNS (Multicast DNS) responder
      if (MDNS.begin(device_name)) {
        ////DPRINTLN("mDNS responder started");
        MDNS.addService("_apple-midi", "_udp", 5004);
      }

      ipMIDI.beginMulticast(ipMIDImulticast, ipMIDIdestPort);
      ////DPRINTLN("ipMIDI server started");

      // RTP-MDI
      apple_midi_start();
      ////DPRINTLN("RTP-MIDI started");
      break;

    case SYSTEM_EVENT_STA_LOST_IP:
      ////DPRINTLN("SYSTEM_EVENT_STA_LOST_IP");
      MDNS.end();
      ipMIDI.stop();
      break;

    case SYSTEM_EVENT_STA_DISCONNECTED:
      ////DPRINTLN("SYSTEM_EVENT_STA_DISCONNECTED");
      MDNS.end();
      ipMIDI.stop();
      break;

    case SYSTEM_EVENT_AP_START:
      ////DPRINTLN("SYSTEM_EVENT_AP_START");
      break;

    case SYSTEM_EVENT_AP_STOP:
      ////DPRINTLN("SYSTEM_EVENT_AP_STOP");
      break;

    case SYSTEM_EVENT_AP_STACONNECTED:
      ////DPRINTLN("SYSTEM_EVENT_AP_STACONNECTED");
      break;

    case SYSTEM_EVENT_AP_STADISCONNECTED:
      ////DPRINTLN("SYSTEM_EVENT_AP_STADISCONNECTED");
      break;

    case SYSTEM_EVENT_AP_PROBEREQRECVED:
      ////DPRINTLN("SYSTEM_EVENT_AP_PROBEREQRECVED");
      break;

    default:
      ////DPRINTLN("Event: %d", event);
      break;
  }
}
