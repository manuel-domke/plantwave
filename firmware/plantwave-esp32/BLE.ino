class MySecurity : public BLESecurityCallbacks {

    uint32_t onPassKeyRequest() {
      ESP_LOGI(LOG_TAG, "PassKeyRequest");
      return 123456;
    }

    void onPassKeyNotify(uint32_t pass_key) {
      ESP_LOGI(LOG_TAG, "The passkey Notify number:%d", pass_key);
    }

    bool onConfirmPIN(uint32_t pass_key) {
      ESP_LOGI(LOG_TAG, "The passkey YES/NO number:%d", pass_key);
      vTaskDelay(5000);
      return false;
    }
    bool onSecurityRequest() {
      ESP_LOGI(LOG_TAG, "SecurityRequest");
      return false;
    }

    void onAuthenticationComplete(esp_ble_auth_cmpl_t cmpl) {
      leavePairingMode();
      ESP_LOGI(LOG_TAG, "Starting BLE work!");
    }
};

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      bleDeviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      bleDeviceConnected = false;
    }
};

class BLEUARTRXCallback: public BLECharacteristicCallbacks {

    String getValue(String data, char separator, int index)
    {
      int found = 0;
      int strIndex[] = { 0, -1 };
      int maxIndex = data.length() - 1;

      for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
          found++;
          strIndex[0] = strIndex[1] + 1;
          strIndex[1] = (i == maxIndex) ? i + 1 : i;
        }
      }
      return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
    }

    void onWrite(BLECharacteristic *pCharacteristic) {
      String rxValue = pCharacteristic->getValue().c_str();
      if (!midiMode) {
        Serial.printf("*** Received Value (via BLE-UART): %s ***\n", rxValue);
      }

      String command = getValue(rxValue, ' ', 0);
      String setting = getValue(rxValue, ' ', 1);
      String value1 = getValue(rxValue, ' ', 2);
      String value2 = getValue(rxValue, ' ', 3);

      if (command == "set") {
        if (setting == "sense") {
          int intValue = value1.toInt();
          setThreshold(intValue);
          preferences.putInt("sensitivity", intValue);
        }

        // CHANNEL
        if (setting == "channel") {
          int intValue = value1.toInt();
          if ((intValue > 0) && (intValue < 17)) {
            channel = intValue;
            preferences.putInt("channel", intValue);
          }
        }

        // SCALE
        if (setting == "scale") {
          int intValue = value1.toInt();
          if ((intValue >= 0) && (intValue <= 5)) {
            currScale = intValue;
            preferences.putInt("scale", intValue);
          }
        }

        // POLY
        if (setting == "poly") {
          int intValue = value1.toInt();
          if ((intValue >= 0) && (intValue <= 8)) {
            currScale = intValue;
            preferences.putInt("poly", intValue);
          }
        }

        // ROOT
        if (setting == "root") {
          int intValue = value1.toInt();
          if ((intValue >= 0) && (intValue <= 11)) {
            root = intValue;
            preferences.putInt("root", intValue);
          }
        }

        // LED-MODE
        if (setting == "ledmode") {
          int intValue = value1.toInt();
          if ((intValue >= 0) && (intValue <= 2)) {
            noteLEDs = intValue;
            preferences.putInt("led-mode", intValue);
          }
        }

        // USB-MODE
        if (setting == "usbmode") {
          int intValue = value1.toInt();
          if ((intValue >= 0) && (intValue <= 2)) {
            midiMode = intValue;
            preferences.putInt("usb-mode", intValue);
          }
        }

        // LED Brightness
        if (setting == "brightness") {
          int intValue = value1.toInt();
          if ((intValue >= 0) && (intValue <= 255)) {
            maxBrightness = intValue;
            preferences.putInt("led-bright", intValue);
          }
        }

        // controlNumber
        if (setting == "controlNumber") {
          int intValue = value1.toInt();
          if ((intValue >= 0) && (intValue <= 127)) {
            controlNumber = intValue;
            preferences.putInt("controlNumber", intValue);
          }
        }

        // WIFI CONFIG
        if (setting == "wifi") {
          // re-get value1+value2
          if (rxValue.indexOf("\"") > 0) {
            value1 = getValue(rxValue, '"', 1);
            value2 = getValue(rxValue, '"', 3);
          }

          save_wifi_credentials(value1, value2);
          wifi_enabled = true;
          preferences.putBool("wifi-state", true);
          wifi_connect();
        }

        // WIFI STATUS
        if (setting == "wifi-mode") {
          int intValue = value1.toInt();
          if (value1 == 0) {
            disableWifi();
          } else if (intValue == 1) {
            enableWifi();
          }
        }

        // DEVICE NAME
        if (setting == "name") {
          if (isValidDeviceName(value1)) {
            preferences.putString("name", value1);
            delay(100);
            ESP.restart();
          } else {
            sendBLEUARTMessage("err: INVALID_NAME");
          }
        }
      } else if (command == "do") {
        if (setting == "update") {
          doUpdate();
        }
      } else if (command = "get") {
        if (setting == "mac") {
          uint8_t base_mac_addr[6] = {0};
          esp_err_t ret = ESP_OK;
          ret = esp_efuse_mac_get_default(base_mac_addr);
          if (ret != ESP_OK) {
            sendBLEUARTMessage("err: getting mac address failed");
          } else {
            char format[] = "MAC: %x:%x:%x:%x:%x:%x";
            char command[50] = "";
            sprintf(command, format, base_mac_addr[0], base_mac_addr[1], base_mac_addr[2], base_mac_addr[3], base_mac_addr[4], base_mac_addr[5]);
            sendBLEUARTMessage(command                               );
          }
        }
      }
    }
};

void disableWifi() {
  wifi_enabled = false;
  preferences.putBool("wifi-state", false);
  delay(500);
  server.stop();
  esp_wifi_stop();
}

void enableWifi() {
  wifi_enabled = true;
  preferences.putBool("wifi-state", true);
  delay(500);
  ESP.restart();
}

void sendBLEUARTMessage(String message) {
  message +="\n";
  uartTXCharacteristic->setValue(message.c_str());
  uartTXCharacteristic->notify();
}

void doUpdate() {
  cli();
  if (WiFi.getMode() != WIFI_STA) {
    sendBLEUARTMessage("err: WIFI_NOT_CONNECTED");
    sei();
    return;
  }
  preferences.putBool("do-update", true);
  sendBLEUARTMessage("ok: UPDATE_STARTED");

  delay(500);
  ESP.restart();
}

BLESecurity *pSecurity ;

void startBLE() {
  BLEDevice::init(device_name);
  BLEDevice::setEncryptionLevel(ESP_BLE_SEC_ENCRYPT);
  BLEDevice::setSecurityCallbacks(new MySecurity());

  // Create the BLE Server
  BLEServer *pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // pw
  BLEService *pwService = pServer->createService(BLEUUID(PLANTWAVE_SERVICE_UUID));
  BLECharacteristic *pwCharacteristic = pwService->createCharacteristic(
                                          PLANTWAVE_ID_CHARACTERISTIC_UUID,
                                          BLECharacteristic::PROPERTY_READ
                                        );
  pwCharacteristic->addDescriptor(new BLE2902());

  // midi
  BLEService *midiService = pServer->createService(BLEUUID(BLE_MIDI_SERVICE_UUID));
  midiCharacteristic = midiService->createCharacteristic(
                         BLEUUID(BLE_MIDI_CHARACTERISTIC_UUID),
                         BLECharacteristic::PROPERTY_READ   |
                         BLECharacteristic::PROPERTY_WRITE  |
                         BLECharacteristic::PROPERTY_NOTIFY |
                         BLECharacteristic::PROPERTY_WRITE_NR
                       );
  midiCharacteristic->addDescriptor(new BLE2902());

  // uart 0
  BLEService *uartService = pServer->createService(BLE_UART_SERVICE_UUID);
  uartTXCharacteristic = uartService->createCharacteristic(
                           BLE_UART_CHARACTERISTIC_UUID_TX,
                           BLECharacteristic::PROPERTY_NOTIFY
                         );
  uartTXCharacteristic->addDescriptor(new BLE2902());

  BLECharacteristic *uartRXCharacteristic = uartService->createCharacteristic(
        BLE_UART_CHARACTERISTIC_UUID_RX,
        BLECharacteristic::PROPERTY_WRITE
      );

  uartRXCharacteristic->setCallbacks(new BLEUARTRXCallback());
  pwService->start();
  midiService->start();
  uartService->start();
  BLEAdvertising *pAdvertising = pServer->getAdvertising();

  pAdvertising->addServiceUUID(pwService->getUUID());
  pAdvertising->addServiceUUID(uartService->getUUID());
  pAdvertising->addServiceUUID(midiService->getUUID());
  pAdvertising->start();

  pSecurity = new BLESecurity();
  pSecurity->setKeySize();
  pSecurity->setAuthenticationMode(ESP_LE_AUTH_REQ_SC_MITM_BOND);
  pSecurity->setCapability(ESP_IO_CAP_IN);
  pSecurity->setInitEncryptionKey(ESP_BLE_ENC_KEY_MASK | ESP_BLE_ID_KEY_MASK);
  uint8_t rsp_key = ESP_BLE_ENC_KEY_MASK | ESP_BLE_ID_KEY_MASK;

  esp_ble_gap_set_security_param(ESP_BLE_SM_SET_RSP_KEY, &rsp_key, sizeof(uint8_t));
}

void enterPairingMode() {
  pSecurity->setCapability(ESP_IO_CAP_NONE);
  pSecurity->setAuthenticationMode(ESP_LE_AUTH_REQ_SC_MITM);
  pairingMode = true;
}

void leavePairingMode() {
  if (!midiMode) {
    Serial.println("Left pairing mode");
  }
  digitalWrite(STATUS_LED, HIGH);
  pSecurity->setCapability(ESP_IO_CAP_IN);
  pSecurity->setAuthenticationMode(ESP_LE_AUTH_REQ_SC_MITM_BOND);
  pairingMode = false;
}

void clearpairing() {
  int dev_num = esp_ble_get_bond_device_num();

  esp_ble_bond_dev_t *dev_list = (esp_ble_bond_dev_t *)malloc(sizeof(esp_ble_bond_dev_t) * dev_num);
  esp_ble_get_bond_device_list(&dev_num, dev_list);
  for (int i = 0; i < dev_num; i++) {
    esp_ble_remove_bond_device(dev_list[i].bd_addr);
  }

  free(dev_list);
}

void disableBLE() {
  esp_bt_controller_disable();
  esp_bt_controller_deinit();
}

void sendBLEStatusInfo() {
  if (ble_enabled && bleStatusLineInterval < currentMillis) {
    bleStatusLineInterval = currentMillis + 10000; //reset for next check
    String message = "bat \"";
    message += calculateBatPercentage();
    message += "\" w_mode \"";
    message += WiFi.getMode();
    message += "\" w_ssid \"";
    message += WiFi.SSID();
    message += "\" usb \"";
    message += (midiMode ? "1" : "0");
    message += "\" channel \"";
    message += channel;
    message += "\" scale \"";
    message += currScale;
    message += "\" root \"";
    message += root;
    message += "\" poly \"";
    message += polyphony;
    message += "\" sense \"";
    message += sensitivity;
    message += "\" brightness \"";
    message += maxBrightness;
    message += "\" ledmode \"";
    message += noteLEDs;
    message += "\" controlNumber \"";
    message += controlNumber;
    message += "\" name \"";
    message += device_name;
    message += "\" version \"";
    message += VERSION;
    message += "/";
    message += STAGE;
    message += "\"\n";
    uartTXCharacteristic->setValue(message.c_str());
    uartTXCharacteristic->notify();
  }
}
