void ap_mode_start()
{
  //STATUS_LED_OFF();
  WiFi.mode(WIFI_AP);
  if (WiFi.softAP(device_name)) {
    ////DPRINTLN("AP mode started");
    ////DPRINTLN("Connect to 'Pedalino' wireless with no password");
  } else {
    ////DPRINTLN("AP mode failed");
  }
}

void ap_mode_stop()
{
  //STATUS_LED_OFF();

  if (WiFi.getMode() == WIFI_AP) {
    WiFi.softAPdisconnect();
  }
  /*  if (WiFi.softAPdisconnect())
      ////DPRINTLN("AP mode disconnected");
    else
      ////DPRINTLN("AP mode disconnected failed");
    }*/
}

bool smart_config()
{
  // Return 'true' if SSID and password received within SMART_CONFIG_TIMEOUT seconds

  // Re-establish lost connection to the AP
  WiFi.setAutoReconnect(true);

  // Automatically connect on power on to the last used access point
  WiFi.setAutoConnect(true);

  // Waiting for SSID and password from from mobile app
  // SmartConfig works only in STA mode
  WiFi.mode(WIFI_STA);
  WiFi.beginSmartConfig();

  //DPRINT("SmartConfig started");

  for (int i = 0; i < SMART_CONFIG_TIMEOUT && !WiFi.smartConfigDone(); i++) {
    status_blink();
    delay(950);
  }
  if (WiFi.smartConfigDone())
  {
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
    }
    ////DPRINTLN("SSID        : %s", WiFi.SSID().c_str());
    ////DPRINTLN("Password    : %s", WiFi.psk().c_str());
    delay(1000);
    save_wifi_credentials(WiFi.SSID(), WiFi.psk());
  }
  //else
  //DPRINT("SmartConfig timeout");

  if (WiFi.smartConfigDone())
  {
    WiFi.stopSmartConfig();
    return true;
  }
  else
  {
    WiFi.stopSmartConfig();
    return false;
  }
}

bool ap_connect(String ssid = "", String password = "")
{
  // Return 'true' if connected to the access point within WIFI_CONNECT_TIMEOUT seconds
  //  int address = 32;
  ssid = preferences.getString("ssid", "");
  //address += ssid.length() + 1;
  password = preferences.getString("psk", "");

  ////DPRINTLN("Connecting to");
  ////DPRINTLN("SSID        : %s", ssid.c_str());
  ////DPRINTLN("Password    : %s", password.c_str());

  if (ssid.length() == 0) return false;

  WiFi.disconnect();
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid.c_str(), password.c_str());
  for (byte i = 0; i < WIFI_CONNECT_TIMEOUT * 2 && !WiFi.isConnected(); i++) {
    status_blink();
    delay(100);
    status_blink();
    delay(300);
  }

  //WiFi.isConnected() ? STATUS_LED_ON() : STATUS_LED_OFF();

  return WiFi.isConnected();
}


bool auto_reconnect(String ssid = "", String password = "")
{
  // Return 'true' if connected to the (last used) access point within WIFI_CONNECT_TIMEOUT seconds
  if (ssid.length() == 0) {
    ssid = preferences.getString("ssid", "");
    password = preferences.getString("psk", "");
  }

  ////DPRINTLN("Connecting to");
  ////DPRINTLN("SSID        : %s", ssid.c_str());
  ////DPRINTLN("Password    : %s", password.c_str());

  if (ssid.length() == 0) return false;

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid.c_str(), password.c_str());
  for (byte i = 0; i < WIFI_CONNECT_TIMEOUT * 2 && !WiFi.isConnected(); i++) {
    status_blink();
    delay(100);
    status_blink();
    delay(300);
  }

  //WiFi.isConnected() ? STATUS_LED_ON() : STATUS_LED_OFF();

  return WiFi.isConnected();
}

void wifi_connect()
{
  if (!auto_reconnect()) {       // WIFI_CONNECT_TIMEOUT seconds to reconnect to last used access point
    //delay(10000);
    /*
        //if (!WiFi.isConnected()) {
        ////DPRINTLN("Wifi connect failed. Trying smart config");
        if (smart_config()) {       // SMART_CONFIG_TIMEOUT seconds to receive SmartConfig parameters
          ////DPRINTLN("smart config sucess");
          ap_connect();
          auto_reconnect();        // WIFI_CONNECT_TIMEOUT seconds to connect to SmartConfig access point
          delay(10000);
        }
        // }*/
    if (!WiFi.isConnected()) {
      ////DPRINTLN("switching to AP mode until next reboot");
      ap_mode_start();           // switch to AP mode until next reboot
    }
  }
}
