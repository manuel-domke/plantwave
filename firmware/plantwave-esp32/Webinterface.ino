void setupHTTPServer() {
  server.on("/", handleRoot);
  server.on("/update", handleUpdate);
  server.on("/restart", handleRestart);
  server.on("/set/{}", handleSet);
  server.on("/get/{}", handleGet);
  server.on("/factory-reset", handleFactoryReset);
  server.on("/clear-ble", handleClearPairing);
  //server.on("/css", handleCSS);
  server.begin();
}


void handleClearPairing() {
  clearpairing();
  String message = "<html><head><title>Redirect</title><meta http-equiv=\"refresh\" content=\"5; url=/\" /></head><body>Cleared all BLE pairings! <br>Redirecting to homepage in 5s.</body></html>";
  server.send(200, "text/html", message);
}

void handleFactoryReset() {
  loadFactorySettings();
  String message = "<html><head><title>Redirect</title><meta http-equiv=\"refresh\" content=\"5; url=/\" /></head><body>Restored factory settings. Rebooting... <br>Redirecting to homepage in 5s.</body></html>";
  server.send(200, "text/html", message);
  delay(100);
  ESP.restart();
}

void handleRestart() {
  String message = "<html><head><title>Redirect</title><meta http-equiv=\"refresh\" content=\"5; url=/\" /></head><body>Rebooting. <br>Redirecting to homepage in 5s.</body></html>";
  server.send(200, "text/html", message);
  delay(100);
  ESP.restart();
}

HTTPClient http;
void handleUpdate() {
  cli();
  if (WiFi.getMode() == WIFI_AP) {
    String message = "<html><head><title>Redirect</title><meta http-equiv=\"refresh\" content=\"60; url=/\" /></head><body><h1>Update impossible</h1><br>Please connect the device to a wireless network with Internet access.</body></html>";
    server.send(200, "text/html", message);
    sei();
    return;
  }

  String url = "http://";
  url += OTA_UPDATE_BASE_URL;
  url += STAGE;
  url += ".php?md5";

  http.begin(url);
  int httpCode = http.GET();
  String payload;
  if (httpCode > 0) { //Check for the returning code
    payload = http.getString();
    Serial.println(httpCode);
    Serial.println(payload);
    http.end();
  }  else {
    String message = "<html><head><title>Error checking for updates!</title><meta http-equiv=\"refresh\" content=\"60; url=/\" /></head><body>An error occurred when checking the update server. Code: ";
    message += httpCode;
    message += "<br>Redirecting to homepage in 60s.</body></html>";
    server.send(200, "text/html", message);
    sei();
    return;
  }
  if (payload != ESP.getSketchMD5()) {
    preferences.putBool("do-update", true);
    String message = "<html><head><title>Update started!</title><meta http-equiv=\"refresh\" content=\"60; url=/\" /></head><body>Update started. Rebooting. <br>Redirecting to homepage in 60s.</body></html>";
    server.send(200, "text/html", message);
    delay(500);
    ESP.restart();
  } else {
    String message = "<html><head><title>Already up to date!</title><meta http-equiv=\"refresh\" content=\"5; url=/\" /></head><body>Your PlantWave is already up to date! (v";
    message += VERSION;
    message += ")<br>Redirecting to homepage in 5s.</body></html>";
    server.send(200, "text/html", message);
    sei();
  }
}

void handleSet() {
  String message = "<html><head><title>Settings Redirect</title><meta http-equiv=\"refresh\" content=\"3; url=/\" /></head><body>";
  String messageEnd = "</body></html>";
  String setting = server.pathArg(0);
  String value1, value2;

  for (uint8_t i = 0; i < server.args(); i++) {
    String argname = server.argName(i);
    String argvalue = server.arg(i);
    if (argname == "value1") {
      value1 = argvalue;
    } else if (argname = "value2") {
      value2 = argvalue;
    }
  }

  if (setting == "ledmode") {
    int intValue = value1.toInt();
    if ((intValue >= 0) && (intValue <= 2)) {
      noteLEDs = intValue;
      preferences.putInt("led-mode", intValue);
      message += "OK";
      message += messageEnd;
      server.send(200, "text/html", message);
    } else {
      message += "NOT OK";
      message += messageEnd;
      server.send(403, "text/html", message);
    }
  } else  if (setting == "channel") {
    int intValue = value1.toInt();
    if ((intValue > 0) && (intValue < 17)) {
      channel = intValue;
      preferences.putInt("channel", intValue);
      message += "OK";
      message += messageEnd;
      server.send(200, "text/html", message);
    } else {
      message += "NOT OK";
      message += messageEnd;
      server.send(403, "text/html", message);
    }
  } else  if (setting == "root") {
    int intValue = value1.toInt();
    if ((intValue >= 0) && (intValue <= 11)) {
      root = intValue;
      preferences.putInt("root", intValue);
      message += "OK";
      message += messageEnd;
      server.send(200, "text/html", message);
    } else {
      message += "NOT OK";
      message += messageEnd;
      server.send(403, "text/html", message);
    }
  } else  if (setting == "controlNumber") {
    int intValue = value1.toInt();
    if ((intValue > 0) && (intValue <= 255)) {
      controlNumber = intValue;
      preferences.putInt("controlNumber", intValue);
      message += "OK";
      message += messageEnd;
      server.send(200, "text/html", message);
    } else {
      message += "NOT OK";
      message += messageEnd;
      server.send(403, "text/html", message);
    }
  } else if (setting == "brightness") {
    int intValue = value1.toInt();
    if ((intValue > 0) && (intValue <= 255)) {
      maxBrightness = intValue;
      preferences.putInt("led-bright", intValue);
      message += "OK";
      message += messageEnd;
      server.send(200, "text/html", message);
    } else {
      message += "NOT OK";
      message += messageEnd;
      server.send(403, "text/html", message);
    }
  }
  else if (setting == "usbmode") {
    int intValue = value1.toInt();
    if (intValue == 0) {
      digitalWrite(USB_MODE_PIN, 0);
      preferences.putInt("usb-mode", MIDI_MODE_DISABLED);
      midiMode = MIDI_MODE_DISABLED;
      // toogle usbmode
      // serial.end + begin instead of restart?
      message += "OK, disabled MIDI/USB-Serial (TRS MIDI and USB-MIDI disabled)<br>Rebooting ESP32...";
      message += messageEnd;
      server.send(200, "text/html", message);
    } else if (intValue == 1) {
      digitalWrite(USB_MODE_PIN, 1);
      preferences.putInt("usb-mode", MIDI_MODE_ENABLED);
      // toogle usbmode
      // serial.end + begin instead of restart
      message += "OK, enabled MIDI mode (TRS MIDI and USB-MIDI activated!)<br>Rebooting ESP32.<br><b>Important:</b>Also replug the USB cable for the USB-MIDI device to be recognized properly!";
      message += messageEnd;
      server.send(200, "text/html", message);
      delay(100);
      ESP.restart();
    } else if (intValue == 2) {
      digitalWrite(USB_MODE_PIN, 0);
      preferences.putInt("usb-mode", MIDI_MODE_USB);
      // toogle usbmode
      // serial.end + begin instead of restart
      message += "OK, enabled USB-serial mode. Rebooting ESP32.<br><b>Important:</b>Also replug the USB cable for the USB-Serial device to be recognized properly!";
      message += messageEnd;
      server.send(200, "text/html", message);
      delay(100);
      ESP.restart();
    }
  } else if (setting == "blestate") {
    int intValue = value1.toInt();
    if (intValue == 0) {
      ble_enabled = false;
      preferences.putBool("ble-state", false);
      //esp_bluedroid_disable();
      //esp_bluedroid_deinit();
      esp_bt_controller_disable();
      esp_bt_controller_deinit();
      message += "OK, disabled BLE!";
      message += messageEnd;
      server.send(200, "text/html", message);
      delay(500);
      ESP.restart();
    } else if (intValue == 1) {
      ble_enabled = true;
      preferences.putBool("ble-state", true);
      esp_bt_controller_enable(ESP_BT_MODE_BLE); // BLE
      startBLE();
      message += "OK, enabled BLE! Restarting ESP32!";
      message += messageEnd;
      server.send(200, "text/html", message);
    }
  } else if (setting == "wifistate") {
    int intValue = value1.toInt();
    if (intValue == 0) {
      wifi_enabled = false;
      preferences.putBool("wifi-state", false);
      message += "OK, Disabling WiFi. You can only re-enable it via a manual factory reset (hold down power button during power-on for 10s)";
      message += messageEnd;
      server.send(200, "text/html", message);
      delay(500);
      server.stop();
      esp_wifi_stop();
    }
  } else if (setting == "wifi") {
    save_wifi_credentials(value1, value2);
    message += "Saved Wifi credentials for SSID: ";
    message += value1;
    message += ". Rebooting ESP32 now.";
    message += messageEnd;
    server.send(200, "text/html", message);
    delay(500);
    ESP.restart();
  } else if (setting == "scale") {
    int intValue = value1.toInt();
    if ((intValue >= 0) && (intValue <= scaleCount)) {
      preferences.putInt("scale", intValue);
      currScale = intValue;
      message += "OK";
      message += messageEnd;
      server.send(200, "text/html", message);
    } else {
      message += "NOT OK";
      server.send(403, "text/plain", "NOT OK");
    }
  } else if (setting == "poly") {
    int intValue = value1.toInt();
    if ((intValue >= 1) && (intValue <= MAX_POLYPHONY)) {
      preferences.putInt("poly", intValue);
      polyphony = intValue;
      message += "OK - Set polyphony to: ";
      message += polyphony;
      message += messageEnd;
      server.send(200, "text/html", message);
    } else {
      message += "NOT OK";
      server.send(403, "text/plain", "NOT OK");
    }
  } else if (setting == "leds") {
    int intValue = value1.toInt();
    if ((intValue > 0) && (intValue <= 2)) {
      noteLEDs = intValue;
      server.send(200, "text/plain", "OK");
    } else {
      server.send(403, "text/plain", "NOT OK");
    }
  } else if (setting == "sensitivity") {
    int intValue = value1.toInt();
    setThreshold(intValue);
    preferences.putInt("sensitivity", intValue);
    message += "OK. Set sensitivity to " ;
    message += intValue;
    message += "%";
    message += messageEnd;
    server.send(200, "text/html", message);
  } else if (setting == "name") {
    if (isValidDeviceName(value1)) {
      preferences.putString("name", value1);
      message = "<html><head><title>Settings Redirect</title><meta http-equiv=\"refresh\" content=\"3; url=http://";
      message += value1;
      message += ".local\" /></head><body>";
      message += "OK. Set device name to " ;
      message += value1;
      message += ". Restarting ESP32";
      message += messageEnd;
      server.send(200, "text/html", message);
      delay(100);
      ESP.restart();
    } else {
      message += "Invalid name: ";
      message += value1;
      message += messageEnd;
      server.send(200, "text/html", message);
    }
  } else {
    server.send(404, "text/plain", "unknown setting");
  }

}


void handleGet() {
  String setting = server.pathArg(0);
  if (setting == "battery") {
    server.send(200, "text/plain", String(batVoltage));
  } else if (setting == "channel") {
    server.send(200, "text/plain", String(channel));
  } else if (setting == "scale") {
    server.send(200, "text/plain", String(currScale));
  } else if (setting == "wifi-mode") {
    String result;
    switch (WiFi.getMode()) {
      case WIFI_STA:
        result = "STATION";
        break;
      case WIFI_AP:
        result = "SOFTAP";
        break;
    }
    server.send(200, "text/plain", result);
  } else if (setting == "wifi-ssid") {
    server.send(200, "text/plain", String(WiFi.SSID()));
  } else if (setting == "led-state") {
    server.send(200, "text/plain", String(noteLEDs));
  } else if (setting == "ble-state") {
  } else if (setting == "ipmidi-state") {
  } else if (setting == "applemidi-state") {
  } else if (setting == "applemidi-connected") {
    server.send(200, "text/plain", String(appleMidiConnected));
  } else if (setting == "ble-connected") {
    server.send(200, "text/plain", String(bleDeviceConnected));
  } else {
    server.send(404, "text/plain" "not found");
  }
}


void handleRoot() {
  // Display the HTML web page

  String message = "<!DOCTYPE html>\
<html lang=\"en\">\
<head>\
  <title>PlantWave Controls</title>\
  <meta charset=\"utf-8\">\
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">";
  if (WiFi.getMode() == WIFI_STA) {
    message += "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\">\
    <link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">\
  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>\
  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\"></script>\
  <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>";

    message += "<script>\
$( function() {\
    $( \"#slider-sensitivity\" ).slider({\
        range: \"max\",\
        min: 1,\
        max: 100,\
        value: ";
    message += sensitivity;
    message += ",\
        stop: function(event, ui) {\
            $(\"#amount\").val(ui.value);\
            $.ajax({\
                method: \"GET\",\
                url: \"/set/sensitivity\",\
                data: { value1: ui.value }\
            })\
        }\
    });\
    $( \"#amount\" ).val( $(\"#slider-sensitivity\").slider(\"value\") );\
});\
</script>";
  }
  message += "<style>\
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */\
    .row.content {height: 1500px}\
    /* Set gray background color and 100% height */\
    .sidenav {\
      background-color: #f1f1f1;\
      height: 100%;\
    }\
    /* Set black background color, white text and some padding */     \
    footer {\
      background-color: #555;    \
      color: white;\
      padding: 15px;\
    }\
    /* On small screens, set height to 'auto' for sidenav and grid */\
    @media screen and (max-width: 767px) {\
      .sidenav {\
        height: auto;\
        padding: 15px;\
      }  \
      .row.content {height: auto;}\
    }\
  </style>\
</head>\
<body>\
<div class=\"container-fluid\">\
  <div class=\"row content\">";
  if (WiFi.getMode() == WIFI_STA) {
    message += "<div class=\"col-sm-2 sidenav\">\
      <h4>PlantWave Controls</h4>\
      <ul class=\"navbar-nav nav nav-pills nav-stacked\">\
        <li><a href=\"#main\">Main controls</a></li>\
    <li><a href=\"#settings\">Settings</a></li>\
        <li><a href=\"#midi\">MIDI</a></li>\
        <li><a href=\"#danger\">Danger Zone</a></li>\
      </ul><br>\
    </div>";

    message += "<div class=\"col-sm-9\">\
<hr><a name=\"main\"><h2>Main controls</h2></a>\
 <div class=\"row\">\
  <div class=\"col-sm-4\">\
    <a href=\"/update\" class=\"btn btn-info\" role=\"button\">Check for Updates</a>\
  </div>\
  <div class=\"col-sm-4\">\
    <a href=\"/restart\" class=\"btn btn-danger\" role=\"button\">Restart</a>\
  </div>\
  <div class=\"col-sm-4\">\
  ";

    // bluetooth
    if (ble_enabled) {
      message += "<a href=\"/set/blestate?value1=0\" class=\"btn btn-danger\" role=\"button\">Disable bluetooth</a>";
    } else {
      message += "<a href=\"/set/blestate?value1=1\" class=\"btn btn-success\" role=\"button\">Enable bluetooth</a>";
    }
    message += "</div>\
  </div>";

    // sensitivity
    message += "<h3>Sensitivity</h3> \
  <div class = \"form-group\">\
  <div id = \"slider-sensitivity\"/>\
  </div>";
  }
  // battery level
  message += "<h3>Battery level </h3>\
                  <div class = \"progress\">";
  float battPercentage =  calculateBatPercentage();
  if (battPercentage > 100) {
    battPercentage = 100;
  }
  message += batVoltage;
  String divclass;
  if (battPercentage < 25) {
    divclass  = "bg-danger";
  } else if (battPercentage < 50) {
    divclass  = "bg-warning";
  } else if (battPercentage < 75) {
    divclass  = "bg-info";
  } else if (battPercentage <= 100) {
    divclass  = "bg-success";
  }
  message += "<div class=\"progress-bar ";
  message += divclass;
  message += "\" role=\"progressbar\" style=\"width: ";
  message += battPercentage;
  message += "%\" aria-valuenow=\"";
  message += battPercentage;
  message += "\" aria-valuemin=\"0\" aria-valuemax=\"100\">";
  message += battPercentage;
  message += " % </div>";
  message += "</div>";

  if (WiFi.getMode() == WIFI_AP) {
    message += "<h3>Please enter your WiFi credentials to enable full access to this webinterface</h3>";
  }

  // settings - wifi/hostname
  message += "<hr>\
      <a name=\"settings\"><h2>Settings</h2></a>\
      <p>basic device configuration like device name, WiFi credentials</p>\
      <div class=\"row\">\
 <div class=\"col-sm-4\">\
  <h5>WiFi configuration</h5>\
   <form action=\"/set/wifi\">\
    <div class=\"form-group\">\
      <label for=\"ssid\">WiFi name (SSID):</label>\
      <input type=\"text\" class=\"form-control\" id=\"ssid\" name=\"value1\" value=\"";
  message += WiFi.SSID().c_str();
  message += "\">\
    </div>\
    <div class=\"form-group\">\
      <label for=\"psk\">Passphrase (PSK):</label>\
      <input type=\"password\" class=\"form-control\" id=\"psk\" name=\"value2\">\
    </div>\
    <button type=\"submit\" class=\"btn btn-success\">Save</button>\
  </form></div>\
 <div class=\"col-sm-4\">\
<form action=\"/set/name\">\
   <div class=\"form-group\">\
      <label for=\"name\">Device name</label>\
      <input type=\"text\" class=\"form-control\" name=\"value1\" id=\"name\" value=\"";
  message += device_name;
  if (WiFi.getMode() == WIFI_AP) {
    message += "\">\
    </div>";
  } else if (WiFi.getMode() == WIFI_STA) {
    // USB SERIAL/MIDI SWITCH
    message += "\">\
    </div>\
    <button type=\"submit\" class=\"btn btn-success\">Save</button>\
  </form> \
</div>\
</div>\
      <hr>\
      <a name=\"midi\"><h4>MIDI</h4></a>\
      <p>settings specific to the MIDI output options</p>\
  <h5>USB Mode</h5>\
      <form action=\"/set/usbmode\">\
     <div class=\"radio\">\
      <label><input type=\"radio\" name=\"value1\" value=\"0\"";
    if (midiMode == MIDI_MODE_DISABLED) {
      message += " checked";
    }
    message += ">off (3,5mm TRS+USB-MIDI disabled)</label>\
    </div>     <div class=\"radio\">\
      <label><input type=\"radio\" name=\"value1\" value=\"1\"";
    if (midiMode == MIDI_MODE_ENABLED) {
      message += " checked";
    }
    message += ">on (3,5mm TRS+USB-MIDI enabled</label>\
    </div>     <div class=\"radio\">\
      <label><input type=\"radio\" name=\"value1\" value=\"2\"";
    if (midiMode == MIDI_MODE_USB) {
      message += " checked";
    }
    message += ">USB Serial (i.e. firmware updates via USB)</label>\
    </div> \
        <button type=\"submit\" class=\"btn btn-success\">Save</button>\
      </form> \
  <div class=\"row\">\
 <div class=\"col-sm-4\">\
  <h5>Channel</h5>\
  <form action=\"/set/channel\">\
    <div class=\"form-group\">\
     <label for=\"midi-channel\">Channel: (1-16)</label>\
     <input type=\"text\" class=\"form-control\" name=\"value1\" id=\"midi-channel\"  value=\"";
    message += channel;
    message += "\">\
   </div>\
    <button type=\"submit\" class=\"btn btn-success\">Save</button>\
  </form>\
      </div> ";

    // NOTE SCALE
    message += "  <div class=\"col-sm-4\">\
 <h5>Note Scale</h5>\
  <form action=\"/set/scale\">\
    <select name=\"value1\">\
      <option value=\"0\"";
    if (currScale == 0) {
      message += " selected";
    }
    message += ">Chromatic</option> \
             <option value = \"1\"";
    if (currScale == 1) {
      message += " selected";
    }
    message += ">Major</option>\
     <option value=\"2\"";
    if (currScale == 2) {
      message += " selected";
    }
    message += ">Indian</option>\
     <option value=\"3\"";
    if (currScale == 3) {
      message += " selected";
    }
    message += ">Minor</option>\
     <option value=\"4\"";
    if (currScale == 4) {
      message += " selected";
    }
    message += ">Major Pentatonic</option>\
     <option value=\"5\"";
    if (currScale == 5) {
      message += " selected";
    }
    message += ">Minor Pentatonic</option>";
    message += "</select>\
    <button type=\"submit\" class=\"btn btn-success\">Save</button>\
  </form> \
      </div>";

    // POLYPHONY
    message += "  <div class=\"col-sm-4\">\
 <h5>Polyphony</h5>\
  <form action=\"/set/poly\">\
    <select name=\"value1\">\
      <option value=\"1\"";
    if (polyphony == 1) {
      message += " selected";
    }
    message += ">1</option> \
             <option value = \"3\"";
    if (polyphony == 3) {
      message += " selected";
    }
    message += ">3</option> \
             <option value = \"5\"";
    if (polyphony == 5) {
      message += " selected";
    }
    message += ">5</option>\
     <option value=\"8\"";
    if (polyphony == 8) {
      message += " selected";
    }
    message += ">8</option>";
    message += "</select>\
    <button type=\"submit\" class=\"btn btn-success\">Save</button>\
  </form> \
      </div>";

    // LED Brightness
    message += "  <div class=\"col-sm-4\">\
 <h5>LED brightness</h5> (0-255)\
  <form action=\"/set/brightness\">\
    <input type=\"text\" class=\"form-control\" id=\"brightness\" name=\"value1\" value=\"";
    message += maxBrightness;
    message += "\"/><button type=\"submit\" class=\"btn btn-success\">Save</button>\
  </form> \
      </div>";

    // Root Key
    message += "  <div class=\"col-sm-4\">\
 <h5>Root key</h5> (0-11)\
  <form action=\"/set/root\">\
    <input type=\"text\" class=\"form-control\" id=\"root\" name=\"value1\" value=\"";
    message += root;
    message += "\"/><button type=\"submit\" class=\"btn btn-success\">Save</button>\
  </form> \
      </div>";

    // Control-Number
    message += "  <div class=\"col-sm-4\">\
 <h5>MIDI CC </h5> (0-127)\
  <form action=\"/set/controlNumber\">\
    <input type=\"text\" class=\"form-control\" id=\"controlNumber\" name=\"value1\" value=\"";
    message += controlNumber;
    message += "\"/><button type=\"submit\" class=\"btn btn-success\">Save</button>\
  </form> \
      </div></div>";
  }

  // DANGER ZONE
  message += "<hr><a name=\"danger\"></a><h4>Danger Zone</h4>\
  <div class=\"row\">\
 <div class=\"col-sm-4\">\
     <a href=\"/set/wifistate?value1=0\" class=\"btn btn-danger\" role=\"button\">Turn off Wifi (danger)</a>\
    <p>Warning: This webinterface will become unavailable. \
    WiFi can only be re-enabled via Bluetooth app or factory reset (hold down button while powering on)</p>\
    </div>\
    <div class=\"col-sm-4\">\
    <a href=\"/factory-reset\" class=\"btn btn-warning\" role=\"button\">Factory Reset</a>\
      <p>Warning: All settings, including WiFi configuration will be lost.</p>\
    </div>\
    <div class=\"col-sm-4\">\
    <a href=\"/clear-ble\" class=\"btn btn-warning\" role=\"button\">Clear BLE bonds</a>\
    </div>\
  </div>\
      <hr>\
    </div>";

  message += "<footer class=\"container-fluid\">";
  message += "Firmware Version: ";
  message += VERSION;
  message += "/";
  message += STAGE;
  message += "<br>WiFi MAC: ";
  message += WiFi.macAddress();
  message += "</footer>\
             </body>\
             </html>";

  server.send(200, "text/html", message);
}
