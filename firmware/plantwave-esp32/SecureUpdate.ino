#include <HTTPUpdate.h>
#include <HTTPClient.h>

void checkForUpdateFlag() {
  if (preferences.getBool("do-update")) {
    preferences.putBool("do-update", false);
    delay(100);
    OTA_update();
  }
}

// Set time via NTP, as required for x.509 validation
bool setClock() {
  configTime(0, 0, "pool.ntp.org", "time.nist.gov");  // UTC
  if (!midiMode)
    Serial.print(F("Waiting for NTP time sync: "));
  time_t now = time(nullptr);
  int i = 0;
  uint8_t maxRetries = 60;
  while ((now < 8 * 3600 * 2) && i < maxRetries ) {
    yield();
    delay(500);
    if (!midiMode)  Serial.print(F("."));
    now = time(nullptr);
    i++;
  }

  if (i == maxRetries) {
    if (!midiMode) Serial.println("NTP time sync failed");
    return false;
  }

  if (!midiMode) Serial.println(F(""));
  struct tm timeinfo;
  gmtime_r(&now, &timeinfo);
  if (!midiMode)Serial.print(F("Current time: "));
  if (!midiMode)Serial.print(asctime(&timeinfo));
  return true;
}
//
///**
//   This is lets-encrypt-x3-cross-signed.pem
//////*/

// plantwave-fw.crt
const char* rootCACertificate = "-----BEGIN CERTIFICATE-----\n"\
"MIIENzCCAx+gAwIBAgIUGdizyzYRM7xBOdbICzWWFx+Zu6kwDQYJKoZIhvcNAQEL\n"\
"BQAwgakxCzAJBgNVBAYTAkRFMQ8wDQYDVQQIDAZIZXNzZW4xEjAQBgNVBAcMCVdp\n"\
"ZXNiYWRlbjEXMBUGA1UECgwORGF0YUdhcmRlbiBJbmMxEjAQBgNVBAsMCVBsYW50\n"\
"V2F2ZTEjMCEGA1UEAwwaZnd1cGRhdGUuZGF0YWdhcmRlbmNkbi5jb20xIzAhBgkq\n"\
"hkiG9w0BCQEWFG1hbnVlbEBkYXRhZ2FyZGUub3JnMCAXDTIwMDkxMDE4MDgzNFoY\n"\
"DzIxMjAwODE3MTgwODM0WjCBqTELMAkGA1UEBhMCREUxDzANBgNVBAgMBkhlc3Nl\n"\
"bjESMBAGA1UEBwwJV2llc2JhZGVuMRcwFQYDVQQKDA5EYXRhR2FyZGVuIEluYzES\n"\
"MBAGA1UECwwJUGxhbnRXYXZlMSMwIQYDVQQDDBpmd3VwZGF0ZS5kYXRhZ2FyZGVu\n"\
"Y2RuLmNvbTEjMCEGCSqGSIb3DQEJARYUbWFudWVsQGRhdGFnYXJkZS5vcmcwggEi\n"\
"MA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDe+FXUxYJx66ODo6Pg4ScvWj6m\n"\
"Lydkxs9iCud0EgEKcGrFkM0Wxv0LraS3n6yx51sTUhqnbrm9e/+XOeePwg+HRjuV\n"\
"NoSyL1iQN2Nl4JuRhzRImy9t+B9Kxx2Mad/fdyXAU499Sr3FH+Ek0OwJ8kLTRI/H\n"\
"lRH1hLlMZ3vU+faMMajEeVb+wscQTKhd8FMJsVmLhf2R7G65rlx1b9kZtG9qyuI3\n"\
"+eRO9qOC9IMdqGUxbKjduO3nFmMT9rH5R3hNeyY9xz/oyObIFcJFjZXexDPHTUfJ\n"\
"DSrKFluFEdOSwmH01uw5GdhibYWVPNobzoiuxyDrY9LqYCQ0/Ix6wa83uarfAgMB\n"\
"AAGjUzBRMB0GA1UdDgQWBBQwgC6P02Xg2Wd3YakQysZj9zNBEzAfBgNVHSMEGDAW\n"\
"gBQwgC6P02Xg2Wd3YakQysZj9zNBEzAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3\n"\
"DQEBCwUAA4IBAQBvTi2ueOcrXhjLiWe0u0HpA6UYYs3udZvfJ+O1sCAJslIdOGsc\n"\
"B3B1Y2MviikRO67NZVsl7Ue2XQNmjdcRistAQ5xt30ZXgANHvv+8JzVXTE1pjlvm\n"\
"JdmF3yAglWaBmmSPJPGkbCWT0h7cMmbBYh3qDXgQjPskKPttU9ywSVmhW3d2gd9G\n"\
"unRLr6u7VdfwCgppAfnw/FZXPV53+VrVTghWOMYZoJdYLRZX7jK7WCg/TxXmzDli\n"\
"UlNIQPzcX4jbHEh1Y8jxIv0Q+YhnX9uA3AbJC/YnfRf4Ouq7CN0fSzMSXh4RDTDn\n"\
"94wLk2fEIgorD2hF+NXqhw4wgcAd2yUTy4gq\n"\
"-----END CERTIFICATE-----\n";


void OTA_update() {
  cli();

  WiFiClientSecure client1;
  client1.setCACert(rootCACertificate);
  if (!setClock()) {
    if (!midiMode) {
      Serial.println("Setting clock failed");
    }
    blinkLED(0, 3); // fade red LED ON
    return;
  }

  client1.setTimeout(300);
  String url = "https://";
  url += OTA_UPDATE_BASE_URL;
  url += STAGE;
  url += ".php";
  httpUpdate.setLedPin(STATUS_LED, HIGH);
  HTTPUpdateResult ret = httpUpdate.update(client1, url);
  switch (ret) {
    case HTTP_UPDATE_FAILED:
      if (!midiMode) Serial.println("Update failed!");
      blinkLED(0, 5); // fade red LED ON
      break;
    case HTTP_UPDATE_NO_UPDATES:
      if (!midiMode) Serial.println("No updates available!");
      blinkLED(2, 5);
      ESP.restart();
      break;
  }
}
